## Исходное задание

Написать приложение мессенджера на websocket, который должен содержать следующие возможности:

0) Вход по nickname. Если такой пользователь не заходил - создаётся новый. Возможен вход пользователя с разных "приложений". 
1) Вывод список всех пользователей, при появлении нового пользователя, автоматически обновлять список. 
2) Отправка сообщений пользователь-пользователь. В том числе и себе. 
3) Пользователи и сообщения сохраняются в БД. 
4) Сообщение представляет из себя: отправитель, получатель, сообщение, дата отправки.


## Решение 
Для решения за основу взята библиотека https://github.com/gorilla/websocket как наиболее поддерживаемая реализация вебсокетов на Golang, 
в частности пример чата https://github.com/gorilla/websocket/tree/master/examples/chat (BSD-License).

Для фронтенда взят виджет чата https://codepen.io/drehimself/pen/KdXwxR и vue.js. 
 
### Вход по нику
Для простоты и наглядности демо-примера сделана генерация рандомного ника при входе.
 
### Отправка таргетированного сообщения 
Реализовано с помощью именного префикса @username. Разбирается регуляркой в клиентском обработчике чтения из вебсокета, если клиент не найден сообщение выбрасывается (пропускается), если найден - сохраняется в сторадж и уходит в канал unicast хаба. 

### Список пользователей
Обновление списка реализовано отправкой запроса при регистрации/удалении клиента в multicast-канал хаба.

На стороне клиента обработчик получения сообщения из вебсокета смотрит json в теле фрейма, и в зависимости от него уже либо обновляет список пользователей, либо добавляет сообщение.
 
### Сохранение в БД
    
    create database messenger;
    CREATE USER msg_user@'%' IDENTIFIED BY '2mnPSToNaU';
    GRANT ALL ON messenger.* TO msg_user@'%';
    
    CREATE TABLE `users` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `name` varchar(128) NOT NULL,
      `last_seen` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `name` (`name`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    
    CREATE TABLE `messages` (
      `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
      `from` int(10) unsigned NOT NULL,
      `to` int(10) unsigned NOT NULL,
      `body` text NOT NULL,
      `sent_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `from` (`from`),
      KEY `to` (`to`),
      CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`from`) REFERENCES `users` (`id`) ON DELETE CASCADE,
      CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`to`) REFERENCES `users` (`id`) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


### Запуск

    make get
    make install
    ./build/msgr