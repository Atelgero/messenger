// if (window["WebSocket"]) ...
let conn;

let app = new Vue({
    el: '#app',
    data: {
        name: '',
        selected: null,
        messageBuf: null,
        users: [],
        messages: [],
    },
    computed: {
        selectedUsers: function () {
            return this.selected ? [this.selected] : [];
        },
    },
    methods: {
        sendTo: function (target) {
            if (!target) {
                return false;
            }

            let message = '@' + target + ' ' + this.messageBuf;
            conn.send(message);

            if (target === this.name) {
                this.messageBuf = "";
                return
            }

            let msg = {
                body: this.messageBuf,
                source: this.name,
                target: target,
                time: new Date().toLocaleString("ru", options),
            };
            let tgt = this.messages[target] || [];
            tgt.push(msg);
            Vue.set(this.messages, target, tgt);

            this.messageBuf = "";
        },
        append: function (message) {
            let sentTime = Date.parse(message['sent_at']);
            let msg = {
                body: message.body,
                source: message.source,
                target: message.target,
                time: new Date(sentTime).toLocaleString("ru", options),
            };

            let src = this.messages[message.source] || [];
            src.push(msg);
            Vue.set(this.messages, message.source, src);
        },
        isMe: function (user) {
            return user === this.name;
        },
        imagePath: function(user) {
            return user === this.name ?
                'https://web.telegram.org/img/placeholders/Fave.png' :
                'https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01.jpg';
        },
    }
});

const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    weekday: 'short',
    hour: 'numeric',
    minute: 'numeric',
};

window.onload = function () {
    const name = makeid(8);
    app.name = name;
    document.title += (" | " + name);

    conn = new WebSocket("ws://" + document.location.host + "/ws?nickname=" + name);
    conn.onmessage = function (evt) {
        const message = JSON.parse(evt.data);

        if (message.clients !== undefined) {
            app.users = message.clients;
            return
        }

        if (message.body !== undefined) {
            app.append(message);
        }
    };

    // чуть позже, пока не интересует
    /*conn.onclose = function (_) {
        var item = document.createElement("div");
        item.innerHTML = "<b>Connection closed.</b>";
        append(log, item);
    };*/
};

function makeid(len) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

Vue.component('msg-user', {
    props: ['username', 'path'],
    template: `
<li class="clearfix user" @click="$emit('select-user', username)">
    <img :src="path" alt="avatar" />
    <div class="about">
        <div class="name">{{ username}}</div>
        <div class="status">
            <i class="fa fa-circle online"></i> online
        </div>
    </div>
</li>`
});
