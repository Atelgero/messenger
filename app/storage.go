package main

import "time"

type MessageModel struct {
	Body string    `db:"body"`
	From int64     `db:"from"`
	To   int64     `db:"to"`
	Time time.Time `db:"sent_at"`
}

var messageColumns = []string{"body", "from", "to", "sent_at"}

type UserModel struct {
	Name     string    `db:"name"`
	LastSeen time.Time `db:"last_seen"`
}

var userColumns = []string{"name", "last_seen"}

func userSave(nickname string) (id int64, err error) {
	user := UserModel{nickname, time.Now()}

	insertQuery := database.InsertInto("users").Columns(userColumns...)
	insertQuery.Record(user)
	r, err := insertQuery.Exec()
	if err != nil {
		return 0, err
	}

	return r.LastInsertId()
}

func messageSave(from, to int64, body string) (id int64, err error) {
	msgModel := MessageModel{
		Body: body,
		From: from,
		To:   to,
		Time: time.Now(),
	}

	insertQuery := database.InsertInto("messages").Columns(messageColumns...)
	insertQuery.Record(msgModel)
	r, err := insertQuery.Exec()
	if err != nil {
		return 0, err
	}

	return r.LastInsertId()
}
