package main

import (
	"encoding/json"
	"errors"
	"log"
	"time"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte
	unicast   chan Message

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

type Message struct {
	Body   string    `json:"body"`
	Source string    `json:"source"`
	Target string    `json:"target"`
	Time   time.Time `json:"sent_at"`
}

type ClientsList struct {
	Clients []string `json:"clients"`
}

func newHub() *Hub {
	return &Hub{
		//buffered channel to avoid locking in case client := <-h.register
		broadcast:  make(chan []byte, 1),
		unicast:    make(chan Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:

			id, err := userSave(client.nickname)
			if err != nil {
				log.Fatalf("hub: user saving error: %v", err)
				h.clients[client] = false
				continue
			}
			client.id = id
			h.clients[client] = true

			list := ClientsList{} // see request for fix below
			for c := range h.clients {
				list.Clients = append(list.Clients, c.nickname)
			}
			clientsList, _ := json.Marshal(list)
			// updated list of clients is sent to broadcast channel
			h.broadcast <- clientsList

		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		case message := <-h.unicast:
			// todo probably much more efficiently to use map[nickname]Client
			for client := range h.clients {
				if client.nickname != message.Target {
					continue
				}

				msg, _ := json.Marshal(message)
				select {
				case client.send <- msg:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

func (h *Hub) findByName(username string) (int64, error) {
	for c := range h.clients {
		if c.nickname == username {
			return c.id, nil
		}
	}
	return 0, errors.New("hub: client not found")
}
