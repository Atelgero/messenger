package main

import (
	"flag"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/mailru/dbr"
)

var addr = flag.String("addr", ":8080", "http service address")
var database *dbr.Session

// it's not necessary by application-logic, just for working example
// this should be a separated web-server here, e.g. Nginx to serve static
func serveHome(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		http.ServeFile(w, r, "static/index.html")
	case "/custom.css":
		http.ServeFile(w, r, "static/custom.css")
	case "/messenger.js":
		http.ServeFile(w, r, "static/messenger.js")
	default:
		http.Error(w, "Not found", http.StatusNotFound)
	}
}

func initDB(dsn string) {
	eventReceiver := new(dbr.NullEventReceiver)
	conn, err := dbr.Open("mysql", dsn, eventReceiver)
	if err != nil {
		panic(err)
	}

	database = conn.NewSession(eventReceiver)
}

func main() {
	flag.Parse()
	initDB("msg_user:2mnPSToNaU@tcp(127.0.0.1:3306)/messenger")

	hub := newHub()
	go hub.run()

	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})

	log.Println("listening", *addr)
	log.Fatal(http.ListenAndServe(*addr, nil))
}
