get:
	go get ./app

fmt:
	go fmt ./...

vet:
	go vet ./...

install: fmt vet
	go build -o ./build/msgr ./app/
